import logging
from decimal import Decimal
from datetime import datetime, timedelta
from typing import Optional, List, Union

from sqlalchemy import or_, and_, func
from sqlalchemy.orm import Session

from .schema import Account, Party, Transaction, Group, Tag, TagMap, TagGroupMap

logger = logging.getLogger(__name__)


def add_account(session: Session, name: str, hide: Optional[bool] = False, color: Optional[str] = None) -> Account:
    """Add a new account to the database.

    :param session: The database session.
    :param name: The name of the account.
    :param hide: Whether or not this account should be hidden if possible.
    :param color: Color to associate with the account, defined as a six-character hex code, e.g. FFFFFF.
    :return: The newly created account.
    """
    account = Account(name=name, hide=hide, color=color)
    logger.info('Adding new account: {account}'.format(account=account))
    session.add(account)
    return account


def add_party(session: Session, name: str) -> Party:
    """Add a new party to the database.

    :param session: The database session.
    :param name: The name of the party.
    :return: The newly created party.
    """
    party = Party(name=name)
    logger.info('Adding new party: {party}'.format(party=party))
    session.add(party)
    return party


def _get_most_recent_transaction(session: Session, date: datetime) -> Transaction:
    """Get the most recent transaction on a given date.

    :param session: The database session.
    :param date: The date to get the most recent transaction on.
    :return: The most recent transaction on date.
    """
    logger.debug('Getting most recent transaction')
    tx = session.query(Transaction)\
        .filter_by(date=date)\
        .order_by(Transaction.order.desc())\
        .first()
    logger.debug('Found {tx}'.format(tx=tx))
    return tx


def _get_account_transactions_after(session: Session, transaction: Transaction, inclusive: Optional[bool] = False)\
        -> List[Transaction]:
    """Get a list of transactions occurring after a given transaction.

    :param session: The database session.
    :param transaction: The transaction to find transactions after.
    :param inclusive: Whether or not to include transaction.
    :return: A list of transactions occurring after transaction.
    """
    logger.debug('Getting transactions after {tx}'.format(tx=transaction))
    logger.debug('Inclusive: {inclusive}'.format(inclusive=inclusive))
    account_id = transaction.account_id
    date = transaction.date
    order = transaction.order
    if inclusive:
        txs = session.query(Transaction)\
            .filter_by(account_id=account_id)\
            .filter(or_(Transaction.date > date,
                        and_(Transaction.date == date, Transaction.order >= order)))\
            .all()
        logger.debug('Found {txs}'.format(txs=txs))
        return txs
    else:
        txs = session.query(Transaction) \
            .filter_by(account_id=account_id) \
            .filter(or_(Transaction.date > date,
                        and_(Transaction.date == date, Transaction.order > order))) \
            .all()
        logger.debug('Found {txs}'.format(txs=txs))
        return txs


def _get_transactions_on(session: Session, date: datetime) -> List[Transaction]:
    """Get a list of all transactions on a given date.

    :param session: The database session.
    :param date: The date on which to get transactions.
    :return: A list of all transactions on date.
    """
    logger.debug('Getting transactions on {date}'.format(date=date))
    txs = session.query(Transaction)\
        .filter_by(date=date) \
        .order_by(Transaction.order.desc())\
        .all()
    logger.debug('Found {txs}'.format(txs=txs))
    return txs


def add_transaction(session: Session, account: Account, charge: Decimal, date: datetime,
                    alt_date: Optional[datetime] = None, party: Optional[Party] = None, notes: Optional[str] = None,
                    order: Optional[int] = None) -> Transaction:
    """Add a new transaction to the database.

    :param session: The database session.
    :param account: The account this transaction is charged to.
    :param charge: The charge amount of this transaction. Positive to increase the account, negative to decrease.
    :param date: The date on which the transaction was posted.
    :param alt_date: The date on which the transaction actually occurred, if different from date.
    :param party: The party this transaction was paid to / received from.
    :param notes: Miscellaneous notes about the transaction.
    :param order: The order within date this transaction should be sorted. 0-indexed.
    :return: The newly created transaction.
    """
    logger.debug('Begin adding new transaction')

    party_id = party.id if party is not None else None

    # Determine the order
    order = _get_order_and_update(session, date, order)

    # Determine the balance
    balance = get_account_balance(session, account, date=date, order=order)

    # Insert new transaction
    transaction = Transaction(account_id=account.id, charge=charge, party_id=party_id, balance=balance + charge,
                              date=date, alt_date=alt_date, order=order, notes=notes)
    logger.info('Adding new transaction: {tx}'.format(tx=transaction))
    session.add(transaction)

    # Propagate new balance to future
    for future_tx in _get_account_transactions_after(session, transaction):
        logger.debug('Updating future transaction: {tx}'.format(tx=future_tx))
        future_tx.balance = future_tx.balance + charge
        logger.debug('Updated future transaction:  {tx}'.format(tx=future_tx))

    return transaction


def add_group(session: Session, date: datetime, name: Optional[str] = None, party: Optional[Party] = None,
              sub_groups: Optional[List[Group]] = None, transactions: Optional[List[Transaction]] = None,
              notes: Optional[str] = None) -> Group:
    """Add a new group to the database.

    :param session: The database session.
    :param date: The date on which the group transaction was posted.
    :param name: The name of the group.
    :param party: The party the group transaction was paid to / received from.
    :param sub_groups: Existing groups that should be contained by the new group.
    :param transactions: Existing transactions that should be contained by the new group.
    :param notes: Miscellaneous notes about the group transaction.
    :return: The newly created group.
    """
    party_id = None if party is None else party.id
    group = Group(name=name, party_id=party_id, date=date, notes=notes)
    if sub_groups is not None:
        group.child_groups = sub_groups
    if transactions is not None:
        group.child_transactions = transactions

    logger.info('Adding new group: {group}'.format(group=group))
    session.add(group)
    return group


def add_tag(session: Session, name: str) -> Tag:
    """Add a new tag to the database.

    :param session: The database session.
    :param name: The name of the tag.
    :return: The newly created tag.
    """
    tag = Tag(name=name)
    logger.info('Adding new tag')
    session.add(tag)
    return tag


def edit_account(account: Account, name: Optional[str] = None, hide: Optional[bool] = None,
                 color: Optional[str] = None) -> None:
    """Edit an account's data.

    :param account: The account to edit.
    :param name: The new name to set, if supplied.
    :param hide: The new hide value to set, if supplied.
    :param color: The new color value to set, if supplied.
    :return: None.
    """
    logger.info('Editing account: {account}'.format(account=account))
    if name is not None:
        account.name = name
    if hide is not None:
        account.hide = hide
    if color is not None:
        account.color = color
    logger.info('Edited: {account}'.format(account=account))


def edit_party(party: Party, name: Optional[str] = None) -> None:
    """Edit a party's data.

    :param party: The party to edit.
    :param name: The new name to set, if supplied.
    :return: None
    """
    logger.info('Editing party: {party}'.format(party=party))
    if name is not None:
        party.name = name
    logger.info('Edited: {party}'.format(party=party))


def edit_transaction_party(transaction: Transaction, party: Union[Party, None]) -> None:
    """Edit a transaction's party.

    :param transaction: The transaction to edit.
    :param party: The new party to set. (None removes the party.)
    :return: None.
    """
    logger.info('Editing transaction\'s party: {tx}'.format(tx=transaction))
    transaction.party_id = party.id if party is not None else None
    logger.info('Edited transaction\'s party:  {tx}'.format(tx=transaction))


def edit_transaction_alt_date(transaction: Transaction, alt_date: Union[datetime, None]) -> None:
    """Edit a transaction's alt date.

    :param transaction: The transaction to edit.
    :param alt_date: The new alt date to set. (None removes the alt date.)
    :return: None.
    """
    logger.info('Editing transaction\'s alt date: {tx}'.format(tx=transaction))
    transaction.alt_date = alt_date
    logger.info('Edited transaction\'s alt date:  {tx}'.format(tx=transaction))


def edit_transaction_notes(transaction: Transaction, notes: Union[str, None]) -> None:
    """Edit a transaction's notes.

    :param transaction: The transaction to edit.
    :param notes: The new notes to set. (None removes the notes.)
    :return: None.
    """
    logger.info('Editing transaction\'s notes: {tx}'.format(tx=transaction))
    transaction.notes = notes
    logger.info('Edited transaction\'s notes:  {tx}'.format(tx=transaction))


def edit_transaction_account(session: Session, transaction: Transaction, account: Account) -> None:
    """Edit a transaction's account.

    When the new account is set, the new balance will be computed and propagated to all future transactions.
    All future transactions that used the old account will also have their balance updated.

    :param session: The database session.
    :param transaction: The transaction to edit.
    :param account: The new account to set.
    :return: None.
    """
    logger.info('Editing transaction\'s account: {tx}'.format(tx=transaction))
    logger.debug('Getting all transactions after this to subtract the charge')
    for tx in _get_account_transactions_after(session, transaction):
        logger.debug('Subtracting from: {tx}'.format(tx=tx))
        tx.balance = tx.balance - transaction.charge
        logger.debug('New: {tx}'.format(tx=tx))

    balance = get_account_balance(session, account, transaction.date, transaction.order)
    transaction.balance = balance + transaction.charge
    transaction.account_id = account.id

    logger.debug('Getting all transactions after this to add the charge')
    for tx in _get_account_transactions_after(session, transaction):
        logger.debug('Adding to: {tx}'.format(tx=tx))
        tx.balance = tx.balance + transaction.charge
        logger.debug('New: {tx}'.format(tx=tx))

    logger.info('Edited transaction\'s account:  {tx}'.format(tx=transaction))


def edit_transaction_charge(session: Session, transaction: Transaction, charge: Decimal) -> None:
    """Edit a transaction's charge.

    Computes the new balance and propagates it to all future transactions.

    :param session: The database session.
    :param transaction: The transaction to edit.
    :param charge: The new charge to set.
    :return: None.
    """
    logger.info('Editing transaction\'s charge: {tx}'.format(tx=transaction))
    difference = charge - transaction.charge
    transaction.charge = charge
    transaction.balance = transaction.balance + difference

    logger.debug('Getting all transactions after this to update their balances')
    for tx in _get_account_transactions_after(session, transaction):
        logger.debug('Updating future transaction: {tx}'.format(tx=tx))
        tx.balance = tx.balance + difference
        logger.debug('Updated future transaction:  {tx}'.format(tx=tx))

    logger.info('Edited transaction\'s charge:  {tx}'.format(tx=transaction))


def edit_group_name(group: Group, name: Union[str, None]) -> None:
    """Edit a group's name.

    :param group: The group to edit.
    :param name: The new name to set. (None removes the name.)
    :return: None.
    """
    logger.info('Editing group name: {group}'.format(group=group))
    group.name = name
    logger.info('Edited group name:  {group}'.format(group=group))


def edit_group_party(group: Group, party: Union[Party, None]) -> None:
    """Edit a group's party.

    :param group: The group to edit.
    :param party: The new party to set. (None removes the party.)
    :return: None.
    """
    logger.info('Editing group party: {group}'.format(group=group))
    group.party_id = party.id if party is not None else None
    logger.info('Edited group party:  {group}'.format(group=group))


def edit_group_date(group: Group, date: Union[datetime, None]) -> None:
    """Edit a group's date.

    :param group: The group to edit.
    :param date: The new date to set.
    :return: None.
    """
    logger.info('Editing group date: {group}'.format(group=group))
    group.date = date
    logger.info('Edited group date:  {group}'.format(group=group))


def edit_group_notes(group: Group, notes: Union[str, None]) -> None:
    """Edit a group's notes.

    :param group: The group to edit.
    :param notes: The new notes to set. (None removes the notes.)
    :return:
    """
    logger.info('Editing group notes: {group}'.format(group=group))
    group.notes = notes
    logger.info('Edited group notes:  {group}'.format(group=group))


def edit_group_parent(group: Group, parent: Union[Group, None]) -> None:
    """Edit a group's parent.

    :param group: The group to edit.
    :param parent: The new parent to set. (None removes the parent.)
    :return: None.
    """
    logger.info('Editing group parent: {group}'.format(group=group))
    group.parent_id = parent.id if parent is not None else None
    logger.info('Edited group parent:  {group}'.format(group=group))


def edit_transaction_parent(transaction: Transaction, parent: Union[Group, None]) -> None:
    """Edit a transaction's parent group.

    :param transaction: The transaction to edit.
    :param parent: The new parent to set. (None removes the parent.)
    :return: None
    """
    logger.info('Editing transaction parent: {tx}'.format(tx=transaction))
    transaction.parent_id = parent.id if parent is not None else None
    logger.info('Edited transaction parent:  {tx}'.format(tx=transaction))


def edit_tag(tag: Tag, name: str) -> None:
    """Edit a tag's properties.

    :param tag: The tag to edit.
    :param name: The new name of the tag.
    :return: None.
    """
    logger.info('Editing tag: {tag}'.format(tag=tag))
    tag.name = name
    logger.info('Edited tag:  {tag}'.format(tag=tag))


def delete_account(session: Session, account: Account) -> None:
    """Delete an account.

    :param session: The database session.
    :param account: The account to delete.
    :return: None.
    :raise RuntimeError if the account is included in any transaction.
    """
    logger.info('Deleting account: {account}'.format(account=account))
    if len(account.transactions) != 0:
        logger.warning('Attempted to delete account included in transactions')
        raise RuntimeError('Cannot delete account included in transactions')
    session.delete(account)
    logger.info('Deleted account')


def delete_party(session: Session, party: Party) -> None:
    """Delete a party.

    :param session: The database session.
    :param party: The party to delete.
    :return: None.
    :raise RuntimeError if the party is included in any transaction.
    """
    logger.info('Deleting party: {party}'.format(party=party))
    if len(party.transactions) != 0:
        logger.warning('Attempted to delete party included in transactions')
        raise RuntimeError('Cannot delete party included in transactions')
    session.delete(party)
    logger.info('Deleted party')


def delete_transaction(session: Session, transaction: Transaction) -> None:
    """Delete a transaction

    :param session: The database session.
    :param transaction: The transaction to delete.
    :return: None.
    """

    logger.info('Deleting transaction: {tx}'.format(tx=transaction))

    # Delete the tag maps
    for tag_map in session.query(TagMap).filter_by(transaction_id=transaction.id).all():
        logger.debug('Deleting tag map: {tm}'.format(tm=tag_map))
        session.delete(tag_map)

    date = transaction.date

    logger.debug('Setting transaction charge to 0 so it can be deleted')
    edit_transaction_charge(session, transaction, Decimal('0'))
    session.delete(transaction)

    # Modify the order of remaining transactions.
    logger.debug('Fixing the ordering of other transactions on this date')
    _force_fix_date_order(session, date)

    logger.info('Deleted transaction')


def delete_group(session: Session, group: Group) -> None:
    """Delete a group and all it's transactions and sub-groups.

    :param session: The database session.
    :param group: The group to delete.
    :return: None.
    """
    logger.info('Deleting group: {group}'.format(group=group))

    # Delete the tag maps
    for tag_group_map in session.query(TagGroupMap).filter_by(group_id=group.id).all():
        logger.debug('Deleting tag map: {tm}'.format(tm=tag_group_map))
        session.delete(tag_group_map)

    for child_tx in group.child_transactions:
        logger.debug('Deleting child transaction: {tx}'.format(tx=child_tx))
        delete_transaction(session, child_tx)
    for child_group in group.child_groups:
        logger.debug('Deleting child group: {group}'.format(group=child_group))
        delete_group(session, child_group)
    session.delete(group)

    logger.info('Deleted group')


def ungroup(session: Session, group: Group) -> None:
    """Delete a group but leave the transactions and subgroups.

    :param session: The database session.
    :param group: The group to ungroup.
    :return: None.
    """
    logger.info('Ungrouping: {group}'.format(group=group))
    # Delete the tag maps
    for tag_group_map in session.query(TagGroupMap).filter_by(group_id=group.id).all():
        logger.debug('Deleting tag map: {tm}'.format(tm=tag_group_map))
        session.delete(tag_group_map)

    for child_tx in group.child_transactions:
        logger.debug('Removing parent from child transaction: {tx}'.format(tx=child_tx))
        edit_transaction_parent(child_tx, None)
    for child_group in group.child_groups:
        logger.debug('Removing parent from child group: {group}'.format(group=group))
        edit_group_parent(child_group, None)
    session.delete(group)

    logger.info('Ungrouped')


def delete_tag(session: Session, tag: Tag) -> None:
    """Delete a tag.

    :param session: The database session.
    :param tag: The tag to delete.
    :return: None.
    :raise: RuntimeError if the any transactions or groups use the tag.
    """
    logger.info('Deleting tag')

    if len(tag.tag_maps) != 0:
        logger.warning('Attempted to delete tag included in transactions')
        raise RuntimeError('Cannot delete tag included in transactions')
    if len(tag.tag_group_maps) != 0:
        logger.warning('Attempted to delete tag included in groups')
        raise RuntimeError('Cannot delete tag included in groups')
    session.delete(tag)

    logger.info('Deleted tag')


def _force_fix_date_order(session: Session, date: datetime) -> None:
    """Naively change the order of all transactions on a day to increment from 0 to n - 1.

    This is useful after deleting or moving a transaction.

    :param session: The database session.
    :param date: The date to edit the transactions of.
    :return: None.
    """
    logger.debug('Force fixing order within {date}'.format(date=date))
    txs = _get_transactions_on(session, date)
    expected_order = len(txs) - 1
    for tx in txs:
        tx.order = expected_order
        expected_order -= 1


def _get_order_and_update(session: Session, date: datetime, order: Union[int, None]) -> int:
    """Gets the order of a transaction to insert on a day and updates the other transactions on that day if needed.

    :param session: The database session.
    :param date: The date to insert on.
    :param order: The desired order. None for the next natural number.
    :return: The order assign to the transaction.
    """
    logger.debug('Getting order and updating {date}'.format(date=date))

    if order is None:
        logger.debug('Getting next order')
        most_recent_tx = _get_most_recent_transaction(session, date)
        order = most_recent_tx.order + 1 if most_recent_tx is not None else 0
    else:
        other_txs_on_date = _get_transactions_on(session, date)
        if order > len(other_txs_on_date):
            raise RuntimeError('Invalid order for new transaction: {at} ({num} other transactions)'
                               .format(at=order, num=len(other_txs_on_date)))
        logger.debug('Modifying other transactions on this date')
        for tx in other_txs_on_date:
            if tx.order >= order:
                logger.debug('Updating transaction: {tx}'.format(tx=tx))
                tx.order = tx.order + 1
                logger.debug('Updated transaction:  {tx}'.format(tx=tx))

    return order


def move_transaction(session: Session, transaction: Transaction, new_date: datetime,
                     new_order: Optional[int] = None) -> None:
    """Move a transaction to a different date, optionally with specified order within the date.

    (Can be moved to the same date to just change the order)

    :param session: The database session.
    :param transaction: The transaction to move.
    :param new_date: The date to move the transaction to.
    :param new_order: The order to set the transaction in the new date.
    :return: None.
    """
    logger.info('Moving transaction: {tx}'.format(tx=transaction))

    old_date = transaction.date
    charge = transaction.charge

    # Create a temporary date to move the transaction to that is neither the old date or the new date
    temp_date = old_date + timedelta(days=1)
    if temp_date == new_date.date():
        temp_date += timedelta(days=1)

    # Clear the charge so we can move it freely
    logger.debug('Setting charge to 0 as a "soft delete"')
    edit_transaction_charge(session, transaction, Decimal('0'))

    logger.debug('Setting new date to {date} (guaranteed to not be in this move)'.format(date=temp_date))
    transaction.date = temp_date

    # Clean up the order on the old date
    logger.debug('Fixing order on old date with transaction moved out')
    _force_fix_date_order(session, old_date)

    # Determine the order on new date
    logger.debug('Verifying new order on new date')
    new_order = _get_order_and_update(session, new_date, new_order)

    logger.debug('Getting new balance')
    new_balance = get_account_balance(session, transaction.account, date=new_date, order=new_order)

    transaction.date = new_date
    transaction.order = new_order
    transaction.balance = new_balance
    logger.debug('Restoring charge')
    edit_transaction_charge(session, transaction, charge)

    logger.info('Moved transaction:  {tx}'.format(tx=transaction))


def add_tag_to_transaction(session: Session, transaction: Transaction, tag: Tag) -> None:
    """Add a tag to a transaction.

    :param session: The database session.
    :param transaction: The transaction to tag.
    :param tag: The tag to set.
    :return: None.
    """
    logger.info('Adding tag {tag} to transaction {tx}'.format(tag=tag, tx=transaction))
    tag_map = TagMap(tag_id=tag.id, transaction_id=transaction.id)
    session.add(tag_map)


def remove_tag_from_transaction(session: Session, transaction: Transaction, tag: Tag) -> None:
    """Remove a tag from a transaction.

    It is not an error to remove a tag that is not on the transaction.

    :param session: The database session.
    :param transaction: The transaction to remove the tag from.
    :param tag: The tag to remove.
    :return: None.
    """
    logger.info('Removing tag {tag} from transaction {tx}'.format(tag=tag, tx=transaction))
    tag_map = session.query(TagMap).filter_by(transaction_id=transaction.id, tag_id=tag.id).first()
    if tag_map is not None:
        session.delete(tag_map)


def add_tag_to_group(session: Session, group: Group, tag: Tag) -> None:
    """Add a tag to a group.

    :param session: The database session.
    :param group: The group to tag.
    :param tag: The tag to set.
    :return: None.
    """
    logger.info('Adding tag {tag} to group {group}'.format(tag=tag, group=group))
    tag_group_map = TagGroupMap(tag_id=tag.id, group_id=group.id)
    session.add(tag_group_map)


def remove_tag_from_group(session: Session, group: Group, tag: Tag) -> None:
    """Remove a tag from a group.

    It is not an error to remove a tag that is not on the transaction.

    :param session: The database session.
    :param group: The group to remove the tag from.
    :param tag: The tag to remove.
    :return: None.
    """
    logger.info('Removing tag {tag} from group {group}'.format(tag=tag, group=group))
    tag_group_map = session.query(TagGroupMap).filter_by(group_id=group.id, tag_id=tag.id).first()
    if tag_group_map is not None:
        session.delete(tag_group_map)


def get_transaction_tags(transaction: Transaction) -> List[Tag]:
    """Get all tags on a transaction.

    :param transaction: The transaction to get the tags for.
    :return: A list of tags on the transaction.
    """
    logger.debug('Getting tags for transaction: {tx}'.format(tx=transaction))
    tags = [x.tag for x in transaction.tag_maps]
    logger.debug('Found: {tags}'.format(tags=tags))
    return tags


def get_group_tags(group: Group) -> List[Tag]:
    """Get all tags on a group.

    :param group: The group to get the tags for.
    :return: A list of tags on the group.
    """
    logger.debug('Getting tags for group: {group}'.format(group=group))
    tags = [x.tag for x in group.tag_group_maps]
    logger.debug('Found: {tags}'.format(tags=tags))
    return tags


def get_group_charge(group: Group) -> Decimal:
    """Get the total charge from a transaction group.

    :param group: The group of which to compute the total charge.
    :return: The total charge of group.
    """
    logger.debug('Getting total charge of group: {group}'.format(group=group))
    charge = Decimal('0')
    for child_tx in group.child_transactions:
        charge += child_tx.charge
    for child_group in group.child_groups:
        charge += get_group_charge(child_group)
    logger.debug('Charge for group: {group} ({charge})'.format(group=group, charge=charge))
    return charge


def transfer(session: Session, to_account: Account, from_account: Account, charge: Decimal, date: datetime,
             notes: Optional[str] = None) -> None:
    """Create a group that transfers an amount from one account to another.

    :param session: The database session.
    :param to_account: The account to transfer to.
    :param from_account: The account to transfer from.
    :param charge: The amount to transfer, always positive.
    :param date: The date to record the transaction on.
    :param notes: Miscellaneous notes about the transfer.
    :return: None
    """
    # TODO Tag transfer, probably tag both transactions as well as the group
    logger.info('Transferring {charge} from {fro} to {to}'.format(charge=charge, fro=from_account, to=to_account))
    charge = abs(charge)  # Should this be an error?
    logger.debug('Adding negative charge')
    from_tx = add_transaction(session, from_account, -1 * charge, date, notes='Transfer: {notes}'.format(notes=notes))
    logger.debug('Adding positive charge')
    to_tx = add_transaction(session, to_account, charge, date, notes='Transfer: {notes}'.format(notes=notes))
    logger.debug('Adding group')
    add_group(session, date, transactions=[from_tx, to_tx], notes=notes)
    logger.info('Transferred')


def get_account_by_id(session: Session, account_id: int) -> Account:
    """Get an account by its ID.

    SqlAlchemy will raise an error if the account does not exist.

    :param session: The database session.
    :param account_id: The ID of the account.
    :return: The account matching account_id.
    """
    logger.debug('Getting account with ID: {id}'.format(id=account_id))
    account = session.query(Account).filter_by(id=account_id).one()
    logger.debug('Found: {account}'.format(account=account))
    return account


def get_party_by_id(session: Session, party_id: int) -> Party:
    """Get a party by its ID.

    SqlAlchemy will raise an error if the party does not exist.

    :param session: The database session.
    :param party_id: The ID of the party.
    :return: The party matching party_id.
    """
    logger.debug('Getting party with ID: {id}'.format(id=party_id))
    party = session.query(Party).filter_by(id=party_id).one()
    logger.debug('Found: {party}'.format(party=party))
    return party


def get_transaction_by_id(session: Session, transaction_id: int) -> Transaction:
    """Get a transaction by its ID.

    SqlAlchemy will raise an error if the transaction does not exist.

    :param session: The database session.
    :param transaction_id: The ID of the transaction.
    :return: The transaction matching transaction_id.
    """
    logger.debug('Getting transaction with ID: {id}'.format(id=transaction_id))
    transaction = session.query(Transaction).filter_by(id=transaction_id).one()
    logger.debug('Found: {transaction}'.format(transaction=transaction))
    return transaction


def get_group_by_id(session: Session, group_id: int) -> Group:
    """Get a group by its ID.

    SqlAlchemy will raise an error if the transaction does not exist.

    :param session: The database session.
    :param group_id: The ID of the group.
    :return: The group matching group_id.
    """
    logger.debug('Getting group with ID: {id}'.format(id=group_id))
    group = session.query(Group).filter_by(id=group_id).one()
    logger.debug('Found: {group}'.format(group=group))
    return group


def get_tag_by_id(session: Session, tag_id: int) -> Tag:
    """Get a tag by its ID.

    SqlAlchemy will raise an error if the tag does not exist.

    :param session: The database session.
    :param tag_id: The ID of the tag.
    :return: The tag matching tag_id.
    """
    logger.debug('Getting tag with ID: {id}'.format(id=tag_id))
    tag = session.query(Tag).filter_by(id=tag_id).one()
    logger.debug('Found: {tag}'.format(tag=tag))
    return tag


def find_account_by_name(session: Session, search: str) -> List[Account]:
    """Find all accounts whose name matches given search text. Case insensitive.

    Searches only at the beginning of of the name. Meaning a search for 'in' would match 'Insurance', but not 'Golfing'.

    :param session: The database session.
    :param search: The text to search for.
    :return: A list of all accounts whose name matches search.
    """
    logger.debug('Searching for accounts beginning with {search}'.format(search=search))
    accounts = session.query(Account).filter(Account.name.ilike('{search}%'.format(search=search))).all()
    logger.debug('Found: {accounts}'.format(accounts=accounts))
    return accounts


def find_party_by_name(session: Session, search: str) -> List[Party]:
    """Find all accounts whose name matches given search text. Case insensitive.

    Searches only at the beginning of of the name. Meaning a search for 'do' would match 'Dole' but not "McDonald's".

    :param session: The database session.
    :param search: The text to search for.
    :return: A list of all parties whose name matches search.
    """
    logger.debug('Searching for parties beginning with {search}'.format(search=search))
    parties = session.query(Party).filter(Party.name.ilike('{search}%'.format(search=search))).all()
    logger.debug('Found: {parties}'.format(parties=parties))
    return parties


def get_accounts(session: Session) -> List[Account]:
    """Get all accounts.

    :param session: The database session.
    :return: A list of all accounts.
    """
    logger.debug('Getting all accounts')
    accounts = session.query(Account).all()
    logger.debug('Found: {accounts}'.format(accounts=accounts))
    return accounts


def get_parties(session: Session) -> List[Party]:
    """Get all parties.

    :param session: The database session.
    :return: A list of all parties.
    """
    logger.debug('Getting all parties')
    parties = session.query(Party).all()
    logger.debug('Found: {parties}'.format(parties=parties))
    return parties


def get_account_balance(session: Session, account: Account, date: Optional[datetime] = None,
                        order: Optional[int] = None) -> Decimal:
    """Get the balance of an account, optionally at a specific date and order.

    :param session: The database session.
    :param account: The account of which to get the balance.
    :param date: The date on which to get the balance.
    :param order: The order within date on which to get the balance. If date is None, this is ignored.
    :return: The account's balance. The most recent balance if no date specified.
    """
    logger.debug('Getting the balance of account: {account}'.format(account=account))
    if date is not None:
        if order is not None:
            logger.debug('Looking for transactions before {date}, order {order}'.format(date=date, order=order))
            tx = session.query(Transaction) \
                .filter_by(account_id=account.id) \
                .filter(or_(Transaction.date < date,
                            and_(Transaction.date == date, Transaction.order <= order))) \
                .order_by(Transaction.date.desc(), Transaction.order.desc()) \
                .first()
        else:
            logger.debug('Looking for transactions before {date}'.format(date=date))
            tx = session.query(Transaction)\
                .filter_by(account_id=account.id)\
                .filter(Transaction.date <= date)\
                .order_by(Transaction.date.desc(), Transaction.order.desc())\
                .first()
    else:
        logger.debug('Looking for most recent transaction')
        tx = session.query(Transaction) \
            .filter_by(account_id=account.id) \
            .order_by(Transaction.date.desc(), Transaction.order.desc()) \
            .first()

    logger.debug('Found: {transaction}'.format(transaction=tx))

    balance = tx.balance if tx is not None else 0
    logger.debug('Balance: {balance}'.format(balance=balance))
    return balance


def get_transactions(session: Session, accounts: Optional[List[Account]] = None,
                     charge_begin: Optional[Decimal] = None, charge_end: Optional[Decimal] = None,
                     parties: Optional[List[Party]] = None,
                     date_begin: Optional[datetime] = None, date_end: Optional[datetime] = None,
                     debit: Optional[bool] = None, notes_contain: Optional[str] = None,
                     tags: Optional[List[Tag]] = None) -> List[Transaction]:
    """Get a list of all transactions matching certain requirements.

    :param session: The database session.
    :param accounts: List of valid accounts.
    :param charge_begin: The minimum charge (will be treated as absolute value).
    :param charge_end: The maximum charge (will be treated as absolute value).
    :param parties: List of valid parties.
    :param date_begin: The earliest date.
    :param date_end: The latest date.
    :param debit: True for negative charges, False for positive, None for both.
    :param notes_contain: Notes must contain value.
    :param tags: List of valid tags (matches all)
    :return: List of transactions meeting the intersection of all criteria.
    """
    logger.debug('Getting transactions')

    query = session.query(Transaction)

    if accounts is not None:
        account_ids = [a.id for a in accounts]
        query = query.filter(Transaction.account_id.in_(account_ids))
    if charge_begin is not None:
        charge_begin = abs(charge_begin)
        query = query.filter(or_(
            Transaction.charge >= charge_begin, Transaction.charge <= (Decimal('-1') * charge_begin))
        )
    if charge_end is not None:
        charge_end = abs(charge_end)
        query = query.filter(and_(
            Transaction.charge <= charge_end, Transaction.charge >= (Decimal('-1') * charge_end))
        )
    if parties is not None:
        party_ids = [p.id for p in parties]
        query = query.filter(Transaction.party_id.in_(party_ids))
    if date_begin is not None:
        query = query.filter(Transaction.date >= date_begin)
    if date_end is not None:
        query = query.filter(Transaction.date <= date_end)
    if debit is not None:
        if debit:
            query = query.filter(Transaction.charge < Decimal('0'))
        else:
            query = query.filter(Transaction.charge > Decimal('0'))
    if notes_contain is not None:
        query = query.filter(Transaction.notes.ilike('%{notes}%'.format(notes=notes_contain)))

    if tags is not None:
        tag_ids = [x.id for x in tags]
        # We are going to find only transactions with all listed tags. That's how the rest of the criteria work.
        # Maybe later I will change it to allow both intersection and union, but that will be a big change.
        # To do the union, just join TagMap and filter on tag ids in the list.
        # Grouping by transaction ID and finding only those whose count is equal to the number of tags means it
        # will return only those who appear as many times as there are tags, meaning it matches all.
        query = query.join(TagMap)\
            .filter(TagMap.tag_id.in_(tag_ids)) \
            .group_by(Transaction.id) \
            .having(func.count(Transaction.id) == len(tag_ids))

    return query.order_by(Transaction.date.desc(), Transaction.order.desc()).all()


def get_groups(session, charge_begin: Optional[Decimal] = None, charge_end: Optional[Decimal] = None,
               parties: Optional[List[Party]] = None,
               date_begin: Optional[datetime] = None, date_end: Optional[datetime] = None,
               notes_contain: Optional[str] = None, tags: Optional[List] = None) -> List[Group]:
    """Get a list of groups matching certain criteria.

    :param session: The database session.
    :param charge_begin: The minimum charge (will be treated as absolute value).
    :param charge_end: The maximum charge (will be treated as absolute value).
    :param parties: List of valid parties.
    :param date_begin: The earliest date.
    :param date_end: The latest date.
    :param notes_contain: Notes must contain value.
    :param tags: List of valid tags (matches all)
    :return: List of groups meeting the intersection of all criteria.
    """
    logger.info('Getting groups')

    query = session.query(Group)
    if parties is not None:
        party_ids = [p.id for p in parties]
        query = query.filter(Group.party_id.in_(party_ids))
    if date_begin is not None:
        query = query.filter(Group.date >= date_begin)
    if date_end is not None:
        query = query.filter(Group.date <= date_end)
    if notes_contain is not None:
        query = query.filter(Group.notes.ilike('%{notes}%'.format(notes=notes_contain)))

    if tags is not None:
        tag_ids = [x.id for x in tags]
        # See notes in get_transactions
        query = query.join(TagGroupMap)\
            .filter(TagGroupMap.tag_id.in_(tag_ids)) \
            .group_by(Group.id) \
            .having(func.count(Group.id) == len(tag_ids))

    groups = query.order_by(Group.date.desc()).all()

    min_charge = Decimal('-inf')
    if charge_begin is not None:
        min_charge = abs(charge_begin)
    max_charge = Decimal('inf')
    if charge_end is not None:
        max_charge = abs(charge_end)

    return [x for x in groups if min_charge <= abs(get_group_charge(x)) <= max_charge]


def get_transactions_for_tag(session, tag):
    """Get all transactions for a tag.

    This seems to be useless. TODO Delete this

    :param session: The database session.
    :param tag: The tag to get the transactions of.
    :return: A list of transactions tagged with tag.
    """
    return session.query(TagMap).join(Transaction).all()


def get_tag_maps(session):
    """Get all tag maps.

    Primarily useful for testing and debugging.

    :param session: The database session.
    :return: A list of all TagMap rows in the database.
    """
    logger.debug('Getting tag maps')
    maps = session.query(TagMap).all()
    logger.debug('Found: {maps}'.format(maps=maps))
    return maps


def get_tag_group_maps(session):
    """Get all group tag maps.

    Primarily useful for testing and debugging.

    :param session: The database session.
    :return: A list of all TagGroupMap rows in the database.
    """
    logger.debug('Getting tag group maps')
    maps = session.query(TagGroupMap).all()
    logger.debug('Found: {maps}'.format(maps=maps))
    return maps

# ------- Physical accounts??? ---------
# Probably wait for 2.1 on this, plus will give a good way to test migrations
